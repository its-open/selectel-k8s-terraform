variable "sel_token" {
  default = "non-existent-token"
}

variable "project_name" {
  default = "tf-project"
}

variable "cluster_name" {
  default = "tf-cluster"
}

variable "region" {
  default = "ru-2"
}

variable "kube_version" {
  default = "1.23.6"
}

variable "enable_autorepair" {
  default = true
}

variable "enable_patch_version_auto_upgrade" {
  default = true
}

variable "network_id" {
  default = ""
}

variable "subnet_id" {
  default = ""
}

variable "maintenance_window_start" {
  default = ""
}

variable "availability_zone" {
  default = "ru-2b"
}

variable "nodes_count" {
  default = 4
}

variable "keypair_name" {
  default = ""
}

variable "affinity_policy" {
  default = ""
}

variable "cpus" {
  default = 4
}

variable "ram_mb" {
  default = 16384
}

variable "volume_gb" {
  default = 20
}

variable "volume_type" {
  default = "fast.ru-2b"
}

variable "labels" {
  default = {
    "terraform":"true",
  }
}

variable "taints" {
  type = list(object({
    key = string
    value = string
    effect =string
  }))
  default =[]
}

variable "letsencrypt_email" {
  default = "me@mail.com"
}

variable "letsencrypt_server" {
  default = "https://acme-v02.api.letsencrypt.org/directory"
}
