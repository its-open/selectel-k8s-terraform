terraform {
  required_version = ">= 0.13"

  required_providers {
    selectel = {
      source = "selectel/selectel"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}
