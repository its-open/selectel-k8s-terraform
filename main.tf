# Initialize Selectel provider with token.
provider "selectel" {
  token = var.sel_token
}

module "project" {
  source = "./modules/project"

  project_name = var.project_name
}

module "kubernetes_cluster" {
  source = "./modules/cluster"

  cluster_name                      = var.cluster_name
  project_id                        = module.project.project_id
  region                            = var.region
  kube_version                      = var.kube_version
  enable_autorepair                 = var.enable_autorepair
  enable_patch_version_auto_upgrade = var.enable_patch_version_auto_upgrade
  network_id                        = var.network_id
  subnet_id                         = var.subnet_id
  maintenance_window_start          = var.maintenance_window_start
}

module "kubernetes_nodegroup" {
  source = "./modules/nodegroup"

  cluster_id        = module.kubernetes_cluster.cluster_id
  project_id        = module.kubernetes_cluster.project_id
  region            = module.kubernetes_cluster.region
  availability_zone = var.availability_zone
  nodes_count       = var.nodes_count
  keypair_name      = var.keypair_name
  affinity_policy   = var.affinity_policy
  cpus              = var.cpus
  ram_mb            = var.ram_mb
  volume_gb         = var.volume_gb
  volume_type       = var.volume_type
  labels            = var.labels
  taints            = var.taints
}

data "selectel_mks_kubeconfig_v1" "kubeconfig" {
  cluster_id = module.kubernetes_cluster.cluster_id
  project_id = module.kubernetes_cluster.project_id
  region     = module.kubernetes_cluster.region
}

resource "local_file" "kubeconfig_file" {
  content  = data.selectel_mks_kubeconfig_v1.kubeconfig.raw_config
  filename = "${var.cluster_name}.yml"
}

output "project_id" {
  value = module.kubernetes_cluster.project_id
}

output "cluster_id" {
  value = module.kubernetes_cluster.cluster_id
}

provider "helm" {
  kubernetes {
    host                   = data.selectel_mks_kubeconfig_v1.kubeconfig.server
    client_certificate     = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.client_cert)
    client_key             = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.client_key)
    cluster_ca_certificate = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.cluster_ca_cert)
  }
}

resource "helm_release" "nginx-ingress-controller" {
  depends_on = [module.kubernetes_cluster, module.kubernetes_nodegroup]
  name             = "nginx-ingress-controller"
  repository       = "https://charts.bitnami.com/bitnami"
  chart            = "nginx-ingress-controller"
  version          = "9.2.13"
  namespace        = "nginx-ingress-controller"
  create_namespace = true

  set {
    name  = "image.tag"
    value = "1.2.0-debian-10-r0"
  }
}

output "nginx-ingress-controller-metadata" {
  value = helm_release.nginx-ingress-controller.metadata
}

resource "helm_release" "cert-manager" {
  depends_on = [module.kubernetes_cluster, module.kubernetes_nodegroup]
  name             = "cert-manager"
  repository       = "https://charts.jetstack.io"
  chart            = "cert-manager"
  version          = "v1.8.2"
  namespace        = "cert-manager"
  create_namespace = true

  set {
    name  = "installCRDs"
    value = "true"
  }
}

output "cert-manager-metadata" {
  value = helm_release.cert-manager.metadata
}

provider "kubectl" {
  host                   = data.selectel_mks_kubeconfig_v1.kubeconfig.server
  client_certificate     = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.client_cert)
  client_key             = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.client_key)
  cluster_ca_certificate = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.cluster_ca_cert)
  load_config_file       = false
}

resource "kubectl_manifest" "cluster-issuer" {
  depends_on = [helm_release.cert-manager]
  yaml_body = templatefile("${path.module}/cluster-issuer.yaml", {
    email  = var.letsencrypt_email,
    server = var.letsencrypt_server
  })
}


