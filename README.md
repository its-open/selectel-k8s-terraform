# Terraform kubernetes cluster

This repository contains Terraform manifests for the following cluster configuration:
* 3 master nodes
* 4 worker nodes 4 CPU, 16 RAM, 20 SSD 
* Nginx Ingress Controller
* Cert Manager
* Let's Encrypt ClusterIssuer


# Example usage

### Command Line
```sh
terraform init
terraform apply -var "sel_token=your-token" -var "letsencrypt_email=me@mail.com"
terraform destroy
```


### In Docker for Linux

```sh
docker run --rm -it -v $(pwd):/workspace -w /workspace hashicorp/terraform:latest init
docker run --rm -it -v $(pwd):/workspace -w /workspace hashicorp/terraform:latest apply -var "sel_token=your-token" -var "letsencrypt_email=me@mail.com"
docker run --rm -it -v $(pwd):/workspace -w /workspace hashicorp/terraform:latest destroy
```

### In Docker for Windows

```sh
docker run --rm -it -v %cd%:/workspace -w /workspace hashicorp/terraform:latest init
docker run --rm -it -v %cd%:/workspace -w /workspace hashicorp/terraform:latest apply -var "sel_token=your-token" -var "letsencrypt_email=me@mail.com"
docker run --rm -it -v %cd%:/workspace -w /workspace hashicorp/terraform:latest destroy
```

# Yandex terraform mirror
Create terraform.rc in %APPDATA% for Windows or ~/.terraformrc for linux with the following content:
```terraform
provider_installation {
  network_mirror {
    url = "https://terraform-mirror.yandexcloud.net/"
    include = ["registry.terraform.io/*/*"]
  }
  direct {
    exclude = ["registry.terraform.io/*/*"]
  }
}
```